use std::collections::BTreeMap;
use std::cmp::Ord;
use std::clone::Clone;

#[derive(Debug)]
pub struct CharactersFrequency<T> {
    frequencies: BTreeMap<T, u64>,
}

impl<T> CharactersFrequency<T>
where T: Ord + Clone
{
    pub fn new(data: &[T]) -> Self {
        let mut freq = BTreeMap::new();

        for character in data {
            // FIXME le clone est fait quand il faut, mais maintenant on cherche deux fois
            // il faut faire des tests de performance entre l'ancienne et la nouvelle méthode
            // il faut aussi essayer avec un type de map différent

            if freq.contains_key(character) {
                *freq.get_mut(character).unwrap() += 1;
            } else {
                freq.insert(character.clone(), 1);
            }
        }

        CharactersFrequency { frequencies: freq }
    }

    pub fn len(&self) -> usize {
        self.frequencies.len()
    }

    pub fn iter(&self) -> impl Iterator<Item = (&T, &u64)> {
        self.frequencies.iter()
    }

    pub fn freq_of(&self, character: &T) -> Option<&u64> {
        self.frequencies.get(character)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        let cf = CharactersFrequency::new("".as_bytes());
        assert_eq!(cf.len(), 0);
    }

    #[test]
    fn one_letter() {
        let cf = CharactersFrequency::new("a".as_bytes());
        assert_eq!(*cf.freq_of(&b'a').unwrap(), 1);
    }

    #[test]
    fn five_same_letter() {
        let cf = CharactersFrequency::new("aaaaa".as_bytes());
        assert_eq!(*cf.freq_of(&b'a').unwrap(), 5);
    }

    #[test]
    fn different_letters() {
        let cf = CharactersFrequency::new("hannah mur".as_bytes());
        assert_eq!(*cf.freq_of(&b'h').unwrap(), 2);
        assert_eq!(*cf.freq_of(&b'a').unwrap(), 2);
        assert_eq!(*cf.freq_of(&b'n').unwrap(), 2);
        assert_eq!(*cf.freq_of(&b' ').unwrap(), 1);
        assert_eq!(*cf.freq_of(&b'm').unwrap(), 1);
        assert_eq!(*cf.freq_of(&b'u').unwrap(), 1);
        assert_eq!(*cf.freq_of(&b'r').unwrap(), 1);
    }
}
