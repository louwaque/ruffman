use characters_frequency::CharactersFrequency;
use node::Node;
use std::cmp::Ord;
use std::fmt::Display;
use std::clone::Clone;

#[derive(Debug)]
pub struct Tree<T> {
    root: Node<T>,
}

impl<T> Tree<T> {
    pub fn new(cf: &CharactersFrequency<T>) -> Self
    where T: Ord + Clone
    {
        // panic if cf is empty

        let mut nodes = Vec::with_capacity(cf.len());

        for (character, &frequency) in cf.iter() {
            nodes.push(Node::new_leaf(character.clone(), frequency));
        }

        while nodes.len() > 1 {
            nodes.sort_by(|a, b| a.frequency().cmp(&b.frequency()));
            let first = nodes.remove(0);
            let second = nodes.remove(0);
            nodes.push(Node::new_branch(first, second));
        }

        Tree {
            root: nodes.remove(0),
        }
    }

    pub fn root(&self) -> &Node<T> {
        &self.root
    }

    pub fn as_dot(&self) -> String
    where T: Display
    {
        let mut id = 0;
        format!(
            "digraph HuffmanTree {{\nnode [shape = record,height=.1];\n{}\n}}\n",
            self.root.as_dot(&mut id)
        )
    }
}
