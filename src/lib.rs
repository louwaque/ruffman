extern crate bit_vec;
extern crate unicode_segmentation;

pub mod characters_code;
pub mod characters_frequency;
pub mod codec;
pub mod node;
pub mod node_iterator;
pub mod tree;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
