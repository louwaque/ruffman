use bit_vec::BitVec;
use characters_code::CharactersCode;
use characters_frequency::CharactersFrequency;
use node::Node;
use node_iterator::NodeIterator;
use tree::Tree;
use std::cmp::Ord;

pub fn compress<T>(cc: &CharactersCode<T>, data: &[T]) -> Vec<u8>
where T: Ord
{
    // Le premier bit est le ignore_bits
    let mut output = BitVec::from_bytes(&[0]);

    for character in data {
        match cc.get(&character) {
            Some(code) => output.extend(code),
            None => panic!("The character is not found."),
        }
    }

    // let ignore_bits = BitVec::from_bytes(&[255]);
    // for i in 0..output.len() % 8 {
    //     ignore_bits.set(8 - i, false);
    // }

    // Le -8 c'est pour enlever le ignore_bits qui est déjà présent
    // let ignore_bits = 8 - (output.len() - 8) % 8;
    // let mut output = output.to_bytes();
    // println!("ignore_bits : {:?}", ignore_bits);
    //
    // if ignore_bits != 8 {
    //     let ignore_bits = (255 >> ignore_bits) << ignore_bits;
    //     output[0] = ignore_bits;
    // }

    let mut ignore_bits = 8 - (output.len() - 8) % 8;
    if ignore_bits == 8 {
        ignore_bits = 0;
    }
    let mut output = output.to_bytes();
    println!("ignore_bits : {:?}", ignore_bits);
    output[0] = ignore_bits as u8;

    output
}

pub fn uncompress<T>(tree: &Tree<T>, data: &[u8]) -> Vec<T>
where T: Clone
{
    // let ignore_bits = BitVec::from_bytes(&data[0..1]).iter().filter(|x| *x).count();
    // let data = BitVec::from_bytes(&data[1.. data.len() - ignore_bits]);
    let ignore_bits = data[0];
    let data = BitVec::from_bytes(&data[1..]);
    let data_length = data.len() - ignore_bits as usize;
    // Applique le ignore_bits sur le dernier octet de data
    // let last_i = data.len() - 1;
    // let last = data[0] & data[last_i];
    // data.set(last_i, last);

    let mut output = Vec::new();
    let node_it = NodeIterator::new(tree.root(), data.iter().take(data_length));

    for node in node_it {
        if let Node::Leaf { character, .. } = node {
            output.push(character.clone());
        }
    }

    output
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::{Display,Debug};
    use unicode_segmentation::UnicodeSegmentation;

    fn sentences() -> [&'static str; 14] {
        ["a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa",
        "hannah m",
        "this is an example of a huffman tree",
        "Je suis un texte très compliqué avec beaucoup de lettres différentes et des accents ! J'espère que le logiciel sera en mesure de le lire lol.",
        "\" \\ < > { } |",
        "a\r\nb🇷🇺🇸🇹"]
    }

    fn comp_and_decomp<T>(sentence: &[T])
    where T: Ord + Clone + Display + Debug
    {
        let cf = CharactersFrequency::new(&sentence);
        let tree = Tree::new(&cf);
        let cc = CharactersCode::new(&tree);

        println!("sentence   : {:?}", sentence);

        println!("{:#?}", cf);
        println!("{:#?}", tree);
        println!("{}", tree.as_dot());
        println!("{:#?}", cc);

        let data = compress(&cc, &sentence);

        println!("compress   : {:?}", &data[..]);
        println!("compress   : {:?}", BitVec::from_bytes(&data[..]));

        let data = uncompress(&tree, &data[..]);

        println!("uncompress : {:?}", &data[..]);

        assert_eq!(sentence, &data[..]);
    }

    #[test]
    fn comp_and_decomp_u8() {
        for sentence in sentences().iter() {
            comp_and_decomp(sentence.as_bytes());
        }
    }

    #[test]
    fn comp_and_decomp_char() {
        for sentence in sentences().iter() {
            comp_and_decomp(&sentence.chars().collect::<Vec<char>>());
        }
    }

    #[test]
    fn comp_and_decomp_str_graphemes() {
        for sentence in sentences().iter() {
            comp_and_decomp(&sentence.graphemes(true).collect::<Vec<&str>>());
        }
    }
}
