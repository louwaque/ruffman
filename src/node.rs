use std::fmt::Display;

#[derive(Debug)]
pub enum Node<T> {
    Leaf {
        character: T,
        frequency: u64,
    },
    Branch {
        right: Box<Node<T>>,
        left: Box<Node<T>>,
    },
}

impl<T> Node<T> {
    pub fn new_leaf(character: T, frequency: u64) -> Self {
        Node::Leaf {
            character,
            frequency,
        }
    }

    pub fn new_branch(left: Self, right: Self) -> Self {
        Node::Branch {
            left: Box::new(left),
            right: Box::new(right),
        }
    }

    pub fn is_leaf(&self) -> bool {
        match *self {
            Node::Leaf {
                character: _,
                frequency: _,
            } => true,
            _ => false,
        }
    }

    pub fn is_branch(&self) -> bool {
        match *self {
            Node::Branch {
                right: _,
                left: _,
            } => true,
            _ => false,
        }
    }

    pub fn frequency(&self) -> u64 {
        match *self {
            Node::Leaf { frequency, .. } => frequency,
            Node::Branch {
                ref right,
                ref left,
            } => right.frequency() + left.frequency(),
        }
    }

    pub fn as_dot(&self, id: &mut u64) -> String
    where T: Display
    {
        let self_id = *id;
        *id += 1;

        let (font, character, child) = match self {
            Node::Leaf { character, .. } => {
                let character = character.to_string();
                ("fontcolor=red,", match character.as_str() {
                    "\"" | "\\" | "<" | ">" | "{" | "}" | "|" => format!("\\\"\\{}\\\" : ", character),
                    _ => format!("\\\"{}\\\" : ", character),
                }, String::new())
            },
            Node::Branch { right, left, .. } => {
                let (right_id, right_dot) = (*id, right.as_dot(id));
                let (left_id, left_dot) = (*id, left.as_dot(id));
                ("", String::new(), format!("\"{}\":f0 -> \"{}\":f1 [label=\"0\"];\n\"{0}\":f2 -> \"{}\":f1 [label=\"1\"];\n{}{}", self_id, left_id, right_id, left_dot, right_dot))
            },
        };

        format!(
            "\"{}\" [{}label=\"<f0> |<f1> {}{}|<f2> \"];\n{}",
            self_id,
            font,
            character,
            self.frequency(),
            child
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_leaf() {
        let n = Node::new_leaf(b'a', 10);

        assert!(n.is_leaf());
        assert!(!n.is_branch());
    }

    #[test]
    fn new_branch() {
        let n = Node::new_branch(Node::new_leaf(b'a', 1), Node::new_leaf(b'b', 1));

        assert!(n.is_branch());
        assert!(!n.is_leaf());
    }

    #[test]
    fn leaf_frequency() {
        let n = Node::new_leaf(b'a', 10);

        assert_eq!(n.frequency(), 10);
    }

    #[test]
    fn branch_frequency() {
        let n = Node::new_branch(Node::new_leaf(b'a', 10), Node::new_leaf(b'b', 20));

        assert_eq!(n.frequency(), 30);
    }
}
