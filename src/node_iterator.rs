use node::Node;

// Je me demande aussi comment je dois me comporter quand je boucle.
// Cad : quelle noeud donne le premier next : root ou le suivant ?

pub struct NodeIterator<'a, T: 'a, U: Iterator<Item = bool>> {
    root_node: &'a Node<T>,
    current_node: &'a Node<T>,
    path: U,
}

impl<'a, T: 'a, U: Iterator<Item = bool>> NodeIterator<'a, T, U> {
    pub fn new(node: &'a Node<T>, path: U) -> Self {
        NodeIterator { root_node: node, current_node: node, path }
    }
}

impl<'a, T: 'a, U: Iterator<Item = bool>> Iterator for NodeIterator<'a, T, U> {
    type Item = &'a Node<T>;

    fn next(&mut self) -> Option<&'a Node<T>> {
        if let Some(direction) = self.path.next() {
            if let Node::Leaf { .. } = self.current_node {
                self.current_node = self.root_node;
            }

            if let Node::Branch {ref right, ref left, ..} = self.current_node {
                self.current_node = if direction { right } else { left }
            }

            Some(self.current_node)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::cmp::PartialEq;
    use std::fmt::Debug;

    fn check_leaf<T>(node: &Node<T>, valid_character: T, path: impl Iterator<Item = bool>)
    where T: PartialEq + Debug
    {
        let n_it = NodeIterator::new(node, path);
        if let Node::Leaf { character, .. } = n_it.last().unwrap() {
            assert_eq!(valid_character, *character);
        } else {
            panic!("The last node is not a leaf.");
        }
    }

    fn check_branch<T>(node: &Node<T>, path: impl Iterator<Item = bool>) {
        let n_it = NodeIterator::new(node, path);
        match n_it.last().unwrap() {
            Node::Branch { .. } => (),
            _ => panic!("The last node is not a branch."),
        }
    }

    #[test]
    fn one_leaf() {
        let n = Node::new_leaf(b'a', 0);

        check_leaf(&n, b'a', vec![true].into_iter());
        check_leaf(&n, b'a', vec![false].into_iter());
    }

    #[test]
    fn one_branch() {
        let n = Node::new_branch(Node::new_leaf(b'a', 0), Node::new_leaf(b'b', 0));

        check_leaf(&n, b'a', vec![false].into_iter());
        check_leaf(&n, b'b', vec![true].into_iter());
    }

    #[test]
    fn complex_tree() {
        let n = Node::new_branch(
            Node::new_leaf(b'a', 0),
            Node::new_branch(
                Node::new_branch(
                    Node::new_leaf(b'b', 0),
                    Node::new_leaf(b'c', 0)
                ),
                Node::new_branch(
                    Node::new_leaf(b'd', 0),
                    Node::new_leaf(b'e', 0)
                )
            )
        );

        check_leaf(&n, b'a', vec![false].into_iter());
        check_branch(&n, vec![true].into_iter());
        check_branch(&n, vec![true, false].into_iter());
        check_leaf(&n, b'b', vec![true, false, false].into_iter());
        check_leaf(&n, b'c', vec![true, false, true].into_iter());
        check_branch(&n, vec![true, true].into_iter());
        check_leaf(&n, b'd', vec![true, true, false].into_iter());
        check_leaf(&n, b'e', vec![true, true, true].into_iter());
    }
}
