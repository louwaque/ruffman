use node::Node;
use std::collections::BTreeMap;
use tree::Tree;
use bit_vec::BitVec;
use std::cmp::Ord;
use std::clone::Clone;

#[derive(Debug)]
pub struct CharactersCode<T> {
    codes: BTreeMap<T, BitVec>,
}

impl<T> CharactersCode<T>
where T: Ord
{
    pub fn new(tree: &Tree<T>) -> Self
    where T: Clone
    {
        let mut codes = BTreeMap::new();

        if let Node::Leaf { character, .. } = tree.root() {
            codes.insert(character.clone(), BitVec::from_elem(1, false));
        } else {
            let mut nodes = vec![(tree.root(), BitVec::new())];

            while !nodes.is_empty() {
                let mut new_nodes : Vec<(&Node<T>, BitVec)> = Vec::new();

                for (node, mut code) in nodes {
                    match *node {
                        Node::Leaf { ref character, .. } => {
                            codes.insert(character.clone(), code);
                        }
                        Node::Branch {
                            ref left,
                            ref right,
                            ..
                        } => {
                            code.push(false);
                            new_nodes.push((left, code.clone()));

                            let last_bit = code.len() - 1;
                            code.set(last_bit, true);
                            new_nodes.push((right, code));
                        }
                    }
                }

                nodes = new_nodes;
            }
        }

        CharactersCode { codes }
    }

    pub fn len(&self) -> usize {
        self.codes.len()
    }

    pub fn get<'a>(&'a self, character: &T) -> Option<impl 'a + Iterator<Item = bool>> {
        match self.codes.get(character) {
            Some(code) => Some(code.iter()),
            None => None,
        }
    }
}
