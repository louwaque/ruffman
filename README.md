# ruffman

ruffman est un programme qui code et décode le contenu d'un fichier à l'aide du codage de Huffman.
Le codage de Huffman se reposant sur un arbre de fréquences; ruffman permet d'en créer et de les exporter à partir d'un corpus.

## Usage

TODO: Expliquer les fonctionnalités une fois la cli implémentée.

## origine

À l'origine c'est un devoir pour le cours d'algorithme (DUT Info 2018 S3) qui devait être rendu en C++.
Venant tout juste d'apprendre Rust, recoder ce projet me parait être un bon exercice car pour le réaliser je vais devoir :
 - effectuer des opérations sur des fichiers
 - utiliser plusieurs conteneurs standards : liste, tableau associatif
 - réaliser une interface en ligne de commande
 - créer un arbre de fréquences
